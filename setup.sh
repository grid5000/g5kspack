#!/bin/bash
###
# This script defines variables and functions to manage Grid'5000 spack environement.
# It must be sourced in the current shell, as root, e.g.:
#
#   # . /grid5000/spack/develop/g5kspack/setup.sh
#


echo "Loading g5kspack environment."
echo

if [ "$USER" != "root" ]; then
	echo "You are not root, nothing done."
	return 1
fi

if ! ssh-add -L > /dev/null; then
	echo "Warning: you do not have any SSH key available in an SSH agent, remote operations may not work. Mind using \`ssh -A ...' when connecting to this node, then use \`sudo-g5k -sH --preserve-env=SSH_AUTH_SOCK'."
	echo
fi

### First we manage to get the ADM_USER defined so that we can use it to commit in git and more.
declare -a _OAR_JOB_CPUSETS
mapfile -t _OAR_JOB_CPUSETS < <(find /dev/oar_cgroups_links/cpuset/oar/ -mindepth 1 -maxdepth 1 -type d -printf '%f\n' 2> /dev/null)
if [ -n "${_OAR_JOB_CPUSETS[1]}" ] ; then
	# 2 or more jobs...
	echo "More than one job on the node. Cannot continue."
	echo
	return 1
elif [ -z "${_OAR_JOB_CPUSETS[0]}" ]; then
	# No job found
	if [ -n "$ADM_USER" ]; then
		# ADM_USER was apparently set manually but no job is running
		_WARNING="no job is running, node will not be cleaned up automatically."
	else
		echo "No job is running, cannot automatically set ADM_USER. You may set it manually."
		echo
		return 1
	fi
else
	# 1 job found
	ADM_USER=${_OAR_JOB_CPUSETS[0]%_*}
	if ! [ -e /etc/sudoers.d/allowed_by_g5ksudo ]; then
		_WARNING="sudo-g5k was not activated, node will not be cleaned up automatically."
	fi
fi
unset _OAR_JOB_CPUSETS
export ADM_USER
# Force GIT author
export GIT_AUTHOR_NAME=$ADM_USER
export GIT_AUTHOR_EMAIL=$ADM_USER@g5kspack.local
export GIT_COMMITTER_NAME=$ADM_USER
export GIT_COMMITTER_EMAIL=$ADM_USER@g5kspack.local

echo "ADM_USER=$ADM_USER"
echo

set -o pipefail

### g5kspack settings
# source the configuration file given by $G5KSPACK_CONFIG or if not set, in the g5kspack.conf file located
# - in the current directory
# - or in the parent directory as this script
if [ -z "$G5KSPACK_CONFIG" ] && [ -r ./g5kspack.conf ]; then
	G5KSPACK_CONFIG=$(realpath ./g5kspack.conf);
fi
echo "Configuration file is: ${G5KSPACK_CONFIG:=$(realpath "${BASH_SOURCE[0]%${BASH_SOURCE[0]##*/}}"../g5kspack.conf)} (\`unset G5KSPACK_CONFIG' to reset)." #dirname in pure bash
if [ -r "$G5KSPACK_CONFIG" ]; then
	. "$G5KSPACK_CONFIG" || return 1
else
	echo "Error: cannot read $G5KSPACK_CONFIG"
	return 1
fi

# Display config and set default value if need be.
echo "G5KSPACK_UPSTREAM_REMOTE=${G5KSPACK_UPSTREAM_REMOTE:=https://github.com/spack/spack.git}"
echo "G5KSPACK_UPSTREAM_RELEASE=${G5KSPACK_UPSTREAM_RELEASE:=develop}"
echo "G5KSPACK_TOOL_REMOTE=${G5KSPACK_TOOL_REMOTE:=https://gitlab.inria.fr/grid5000/g5kspack.git}"
echo "G5KSPACK_TOOL_RELEASE=${G5KSPACK_TOOL_RELEASE:=main}"
echo "G5KSPACK_REMOTE=${G5KSPACK_REMOTE:=git@gitlab.inria.fr:grid5000/spack.git}"
echo "G5KSPACK_BRANCH=${G5KSPACK_BRANCH:=g5k/${G5KSPACK_UPSTREAM_RELEASE#releases/}}"
echo "G5KSPACK_ROOT=${G5KSPACK_ROOT:=/grid5000/spack/${G5KSPACK_BRANCH#g5k/}}"
echo "G5KSPACK_SERVER=${G5KSPACK_SERVER:=pneyron-spack-vm.grenoble.grid5000.fr}"
echo "G5KSPACK_SSHCMD=${G5KSPACK_SSHCMD:=ssh -o StrictHostKeyChecking=no g5kadmin@$G5KSPACK_SERVER sudo --preserve-env=SSH_CLIENT bash}"
echo "G5KSPACK_NFSEXPORT=${G5KSPACK_NFSEXPORT:=/export/spack/${G5KSPACK_BRANCH#g5k/}_work}"
echo "G5KSPACK_NFSMOUNT_OPTS=${G5KSPACK_NFSMOUNT_OPTS:=}"
echo "G5KSPACK_NFSEXPORT_OPTS=${G5KSPACK_NFSEXPORT_OPTS:=}"
echo "G5KSPACK_DATASET_ORIGIN=${G5KSPACK_DATASET_ORIGIN:=zdata/spack-${G5KSPACK_BRANCH#g5k/}}"
echo "G5KSPACK_DATASET_WORK=${G5KSPACK_DATASET_WORK:=${G5KSPACK_DATASET_ORIGIN}_work}"
echo "G5KSPACK_DEFAULT_COMPILER=${G5KSPACK_DEFAULT_COMPILER:=gcc@10.3.0}"
echo "G5KSPACK_DEFAULT_TARGET=${G5KSPACK_DEFAULT_TARGET:=x86_64_v2}"
echo "G5KSPACK_NFS_SERVERS_SYNC_TRIGGER=${G5KSPACK_NFS_SERVERS_SYNC_TRIGGER:=}"
echo "G5KSPACK_GIT_PUSH=${G5KSPACK_GIT_PUSH:=yes}"
echo

### Source the spack environment.
if [ -r "$G5KSPACK_ROOT/share/spack/setup-env.sh" ]; then
	echo "Load Spack environment setup: $G5KSPACK_ROOT/share/spack/setup-env.sh"
	. $G5KSPACK_ROOT/share/spack/setup-env.sh
	echo
else
	echo "Spack environment setup file not found."
	echo
fi

### _g5kspack_question
# Helper function to ask for a question
# Usage: _g5kspack_question <prompt> <default answer> [<non-interactive answer>]
# Return code is 0 for Y, 1 for N
# default and non-interactive answer must be any of y, n, Y, N
# The non-interactive answer can be omitted
_g5kspack_question() {
	local reply="$3"
	while [ "${reply^}" != "Y" ] && [ "${reply^}" != "N" ]; do
		read -r -p "$1" reply
		if [ -z "$reply" ] && [ -n "$2" ] && { [ "${2^}" == "N" ] || [ "${2^}" == "Y" ]; }; then
			reply=$2
		fi
	done
	if [ "${reply^}" == "Y" ]; then
		return 0
	fi
	return 1
}

### _g5kspack_check_git_clean
# Helper function to check if the spack git repo is clean
# Return code is 1 if not clean.
_g5kspack_check_git_clean() {
	if [ -z "$G5KSPACK_GIT_ALLOW_UNCLEAN" ] && [ -n "$(git -C $G5KSPACK_ROOT status --porcelain)" ]; then
		echo "Spack git repository $G5KSPACK_ROOT is not clean!"
		echo "Please commit or clean changes before installing a package."
		return 1
	fi
}

### _g5kspack_push_to_git
# Helper function to push changes to git remote
_g5kspack_push_to_git() {
	echo "Running \`git -C $G5KSPACK_ROOT push --follow-tags'"
	if ! git -C $G5KSPACK_ROOT push --follow-tags origin; then
		echo
		echo "Git push failed for some reasons. Try again by yourself."
		return 1
	fi
}

### _g5kspack_check_git_remote_sync
# Helper function to check if the spack git repo synched to remote
# Return code is 1 if not synched.
_g5kspack_check_git_remote_sync() {
	if [ -z "$G5KSPACK_GIT_ALLOW_UNCLEAN" ] && ! git -C $G5KSPACK_ROOT diff --quiet "origin/$(git -C $G5KSPACK_ROOT branch --show-current)"; then
		echo "You have git commits that are not pushed to remote:"
		echo
		git -C $G5KSPACK_ROOT log --oneline "origin/$(git -C $G5KSPACK_ROOT branch --show-current)"..HEAD
		echo
		return 1
	fi
}

### g5kspack_rollback_git_origin
# Rollback the git remote to the current local state on the ZFS snapshot (possibly after a ZFS rollback).
# Delete left-over tags
g5kspack_rollback_git_origin() {
	if [ "$G5KSPACK_GIT_PUSH" != "yes" ]; then
		echo "g5kspack is configured to not git push. Nothing to do."
		return 0
	fi
	cat <<EOF
!!!WARNING!!! Use with caution !!!WARNING!!!
This command is meant to be called after a rollback of the ZFS dataset, or if commits/tags were erroneously pushed to origin.
It will roll back the HEAD of the $G5KSPACK_BRANCH on the origin to its current local state in the ZFS dataset (git push -f).
Then it will delete any git tag created by g5kspack_install or g5kspack_uninstall that became orphan after the branch rollback.

EOF
	if git -C $G5KSPACK_ROOT diff --quiet "origin/$(git -C $G5KSPACK_ROOT branch --show-current)"; then
		if ! _g5kspack_question "Local and remote git branches are currently up-to-date. Did you actually rollback anything on ZFS? Continue anyway? [y/N] " N ${G5KSPACK_NON_INTERACTIVE:+N}; then
			echo "Aborted!"
			return 1
		fi
	else
		if ! _g5kspack_question "Really rollback git HEAD of $G5KSPACK_BRANCH on origin to the local state? [Y/n] " Y ${G5KSPACK_NON_INTERACTIVE:+Y}; then
			echo "Aborted!"
			return 2
		fi
		git -C $G5KSPACK_ROOT push -f origin || return 3
	fi
	echo "Try and delete orphan git tags..."
	git -C $G5KSPACK_ROOT fetch origin || return 4
	for entry in $(git -C $G5KSPACK_ROOT for-each-ref --no-merged=remotes/origin/$G5KSPACK_BRANCH --format='%(refname)' refs/tags/$G5KSPACK_BRANCH/); do
		local branch
		branch=$(git -C $G5KSPACK_ROOT branch --format '%(refname)' -r --contains "$entry" | xargs)
		local tag
		tag=${entry#refs/tags/}
		if [ -n "$branch" ]; then
			echo "Found tag $tag however referred by: $branch"
			if _g5kspack_question "Keep it? [Y/n] " Y ${G5KSPACK_NON_INTERACTIVE:+Y}; then
				continue
			fi
		fi
		if _g5kspack_question "Really delete tag $tag? [Y/n] " Y ${G5KSPACK_NON_INTERACTIVE:+Y}; then
			git -C $G5KSPACK_ROOT tag -d "$tag" && git -C $G5KSPACK_ROOT push --delete origin "$tag"
		fi
	done
}

### g5kspack_refresh_modules
# Update environment modules
# Usage: g5kspack_resfresh_modules
g5kspack_refresh_modules() {
	echo "Running \`spack module tcl refresh --delete-tree -y'"
	spack module tcl refresh --delete-tree -y
}

### g5kspack_begin_work
# Create a snapshot of the spack file hierarchy on the server.
# Create a work copy of the spack file hierarchy on the server.
# Export the work copy as NFS, RW to the node, RO to other nodes
# Mount the spack hierarchy on the node.
# Usage: g5kspack_begin_work
g5kspack_begin_work() {
	if $G5KSPACK_SSHCMD <<EOF
set -e
IP=\${SSH_CLIENT%% *}
if ! ping -q -c2 \$IP > /dev/null; then
	echo "Server cannot reach \$IP."
	exit 1
fi
if zfs get origin $G5KSPACK_DATASET_WORK -H -o value >& /dev/null; then
	echo "Work already in progress on \$(host \$(zfs get sharenfs $G5KSPACK_DATASET_WORK -o value -H | grep -oP '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}') | grep ".in-addr.arpa domain name pointer" | sed -e 's/^.* \(.\+\).$/\1/'), $G5KSPACK_DATASET_WORK already exists on the server."
	exit 1
fi
G5KSPACK_DATASET_SNAPSHOT=\$(zfs list -t snapshot | cut -d\  -f1 | grep -P "^$G5KSPACK_DATASET_ORIGIN@\d\d\d\d-\d\d-\d\d_\d\d:\d\d:\d\d$" | tail -n1)
if [ -z "\$G5KSPACK_DATASET_SNAPSHOT" ]; then
	G5KSPACK_DATASET_SNAPSHOT=$G5KSPACK_DATASET_ORIGIN@\$(date +%F_%T)
	echo "Create a first snapshot on remote server: \$G5KSPACK_DATASET_SNAPSHOT"
	zfs snapshot \$G5KSPACK_DATASET_SNAPSHOT
elif [ "\$(zfs get used \$G5KSPACK_DATASET_SNAPSHOT -o value -H)" != "0B" ];then
	echo "Suspicious changes found between $G5KSPACK_DATASET_ORIGIN and \$G5KSPACK_DATASET_SNAPSHOT."
	echo "You may look at the diff on the server with: \\\`zfs diff \$G5KSPACK_DATASET_SNAPSHOT $G5KSPACK_DATASET_ORIGIN' and rollback."
	exit 1
fi
echo "Create work dataset on remote server: $G5KSPACK_DATASET_WORK based on \$G5KSPACK_DATASET_SNAPSHOT"
zfs clone \$G5KSPACK_DATASET_SNAPSHOT $G5KSPACK_DATASET_WORK -o mountpoint=$G5KSPACK_NFSEXPORT -o sharenfs="ro=*,rw=@\$IP/32,no_root_squash${G5KSPACK_NFSEXPORT_OPTS:+,}$G5KSPACK_NFSEXPORT_OPTS"
EOF
	then
		echo "Mount $G5KSPACK_SERVER:$G5KSPACK_NFSEXPORT on $G5KSPACK_ROOT"
		echo "Running \`mount -t nfs ${G5KSPACK_NFSMOUNT_OPTS:+-o $G5KSPACK_NFSMOUNT_OPTS }$G5KSPACK_SERVER:$G5KSPACK_NFSEXPORT $G5KSPACK_ROOT'"
		mkdir -p ${G5KSPACK_ROOT}
		mount -t nfs ${G5KSPACK_NFSMOUNT_OPTS:+-o $G5KSPACK_NFSMOUNT_OPTS }$G5KSPACK_SERVER:$G5KSPACK_NFSEXPORT $G5KSPACK_ROOT
		cd "$PWD" || true
		if [ -e "$G5KSPACK_ROOT/.git" ]; then
			echo "Ready to work in $G5KSPACK_ROOT (git branch is $(git -C $G5KSPACK_ROOT branch --show-current))"
		elif [ -z "$(ls -A $G5KSPACK_ROOT)" ]; then
			echo "Ready to work in $G5KSPACK_ROOT (directory is empty, next step is probably to run g5kspack_init)"
		else
			echo "Error: $G5KSPACK_ROOT should contain a valid GIT repository or be empty"
			return 1
		fi
	else
		echo "Aborting."
		echo "To take over a previous work, run \`g5kspack_take_over_work'."
		echo "To start over, run \`g5kspack_end_work' then again \`g5kspack_begin_work'."
		return 1
	fi
}

### g5kspack_take_over_work
# Take over on the current node of an existing work copy of the spack file hierarchy (RW on the server).
# Mount the spack hierarchy on the node.
# Usage: g5kspack_take_over_work
g5kspack_take_over_work() {
	if $G5KSPACK_SSHCMD <<EOF
set -e
IP=\${SSH_CLIENT%% *}
if ! ping -q -c2 \$IP > /dev/null; then
	echo "Server cannot reach \$IP."
	exit 1
fi
if zfs get origin $G5KSPACK_DATASET_WORK -H -o value >& /dev/null; then
	echo "Update the work dataset NFS export on remote server, RW for \$IP."
	zfs set sharenfs="ro=*,rw=@\$IP/32,no_root_squash${G5KSPACK_NFSEXPORT_OPTS:+,}$G5KSPACK_NFSEXPORT_OPTS" $G5KSPACK_DATASET_WORK
else
	echo "No work in progress: $G5KSPACK_DATASET_WORK does not exist on the server."
	echo "You may run \\\`g5kspack_begin_work'."
	exit 1
fi
EOF
	then
		if grep -q $G5KSPACK_SERVER:$G5KSPACK_NFSEXPORT /proc/mounts; then
			echo "$G5KSPACK_NFSEXPORT is already mounted, remounting it."
			echo "Running \`mount -o remount,rw${G5KSPACK_NFSMOUNT_OPTS:+,$G5KSPACK_NFSMOUNT_OPTS} $G5KSPACK_SERVER:$G5KSPACK_NFSEXPORT'"
			mount -o remount,rw $G5KSPACK_SERVER:$G5KSPACK_NFSEXPORT
		else
			echo "Mount $G5KSPACK_SERVER:$G5KSPACK_NFSEXPORT on $G5KSPACK_ROOT"
			echo "Running \`mount -t nfs ${G5KSPACK_NFSMOUNT_OPTS:+-o $G5KSPACK_NFSMOUNT_OPTS }$G5KSPACK_SERVER:$G5KSPACK_NFSEXPORT $G5KSPACK_ROOT'"
			mount -t nfs ${G5KSPACK_NFSMOUNT_OPTS:+-o $G5KSPACK_NFSMOUNT_OPTS }$G5KSPACK_SERVER:$G5KSPACK_NFSEXPORT $G5KSPACK_ROOT
		fi
		cd "$PWD" || true
		echo "Ready to work in $G5KSPACK_ROOT (git branch is $(git -C $G5KSPACK_ROOT branch --show-current))"
	else
		echo "Abort."
		return 1
	fi
}

### g5kspack_commit_work
# Commit changes made in the work copy, and create a snapshot
# Usage: g5kspack_commit_work
g5kspack_commit_work() {
	if ! grep -q $G5KSPACK_SERVER:$G5KSPACK_NFSEXPORT /proc/mounts; then
		echo "No work in progress locally."
		return 1
	fi
	# Prevent commit if git is not clean
	if ! _g5kspack_check_git_clean; then
		return 1
	fi
	# Refresh modules
	if _g5kspack_question "Refresh modules? [y/N] " N ${G5KSPACK_NON_INTERACTIVE:+N}; then
		g5kspack_refresh_modules
	fi
	# Show ZFS diff with the latest snapshot of the work copy or its origin otherwise
	if _g5kspack_question "Show ZFS diff between work and origin (possibly very long)? [y/N] " N ${G5KSPACK_NON_INTERACTIVE:+N}; then
		$G5KSPACK_SSHCMD <<EOF | less
G5KSPACK_DATASET_LATEST_SNAPSHOT=\$(zfs list -t snapshot | cut -d\  -f1 | grep -P "^$G5KSPACK_DATASET_WORK@\d\d\d\d-\d\d-\d\d_\d\d:\d\d:\d\d$" | tail -n1)
if [ -z "\$G5KSPACK_DATASET_LATEST_SNAPSHOT" ]; then
	G5KSPACK_DATASET_LATEST_SNAPSHOT=\$(zfs get origin $G5KSPACK_DATASET_WORK -o value -H)
fi
echo "Wait for the diff with \$G5KSPACK_DATASET_LATEST_SNAPSHOT to show..." ; zfs diff \$G5KSPACK_DATASET_LATEST_SNAPSHOT $G5KSPACK_DATASET_WORK
EOF
	fi
	# Push changes to git remote
	if [ "$G5KSPACK_GIT_PUSH" == "yes" ]; then
		if ! _g5kspack_check_git_remote_sync; then
			# Push changes to git remote
			if _g5kspack_question "Push git changes (current branch is $(git -C $G5KSPACK_ROOT branch --show-current)) to remote? [y/N] " N ${G5KSPACK_NON_INTERACTIVE:+Y}; then
				if ! _g5kspack_push_to_git; then
					echo "Push failed. Abort."
					return 1
				fi
			else
				echo "Not pushed. Git remote is not synced. Abort."
				return 1
			fi
		else
			echo "Git remote is synced, good."
		fi
	fi
	# Push to ZFS
	if _g5kspack_question "Commit work to remote ZFS dataset and create ZFS snapshot? [y/N] " N ${G5KSPACK_NON_INTERACTIVE:+Y}; then
		sync
		$G5KSPACK_SSHCMD <<EOF
set -e
WORKDIR=\$(zfs get mountpoint $G5KSPACK_DATASET_WORK -H -o value)/
ORIGINDIR=\$(zfs get mountpoint $G5KSPACK_DATASET_ORIGIN -H -o value)/
if [ -z "\$WORKDIR" -o -z "\$ORIGINDIR" ]; then
	echo "Fail to get mount point for $G5KSPACK_DATASET_WORK or $G5KSPACK_DATASET_ORIGIN"
	exit 1
fi
if [ -n "$G5KSPACK_NON_INTERACTIVE" ]; then
	RSYNC_INFO=stats2
else
	RSYNC_INFO=progress2,stats2
fi
echo "Running \\\`rsync -a --info=\$RSYNC_INFO --delete \$WORKDIR \$ORIGINDIR'"

rsync -a --info=\$RSYNC_INFO --delete \$WORKDIR \$ORIGINDIR
echo "Synchronization done."
SNAPSHOT_DATE=\$(date +%F_%T)
# We first create a snapshot of the work copy, usefull for a later diff if work goes on. It will be deleted with the work copy when g5kspack_end_work is called.
# By construction (rsync) both snapshots should contain the same data.
zfs snapshot $G5KSPACK_DATASET_WORK@\$SNAPSHOT_DATE
zfs snapshot $G5KSPACK_DATASET_ORIGIN@\$SNAPSHOT_DATE
echo "Snapshot done: $G5KSPACK_DATASET_ORIGIN@\$SNAPSHOT_DATE"
EOF
	fi
}

### g5kspack_end_work
# Unmount the NFS export and destroy the work copy on the remote server
# Usage: g5kspack_end_work
g5kspack_end_work() {
	pushd / >& /dev/null || true
	if grep -q $G5KSPACK_SERVER:$G5KSPACK_NFSEXPORT /proc/mounts; then
		echo "Umount $G5KSPACK_ROOT"
		if ! umount $G5KSPACK_ROOT; then
			echo "Umount failed, please make sure no process uses the $G5KSPACK_ROOT directory."
			echo "Aborting."
			popd >& /dev/null || true
			return 1
		fi
	else
		echo "No work in progress locally."
	fi
	popd >& /dev/null || true
	sync
	$G5KSPACK_SSHCMD <<EOF
set -e
if ! zfs get origin $G5KSPACK_DATASET_WORK -H -o value >& /dev/null; then
	echo "No work in progress on server."
else
	echo "Destroy work dataset on remote server: $G5KSPACK_DATASET_WORK"
	# Destroy the work copy and any of its snapshot.
	zfs destroy -r $G5KSPACK_DATASET_WORK
fi
EOF
}

### g5kspack_run
# Run a command and log its output in git
# Usage: g5kspack_run <command>
g5kspack_run() {
	# Prevent run if spack's git is not clean
	if ! _g5kspack_check_git_clean; then
		return 2
	fi
	# Do run and log
	local tmpfile
	tmpfile=$(mktemp /tmp/g5kspack_run.XXXX)
cat <<EOF > "$tmpfile"
g5kspack_run $*

# g5kspack_run $*
$ $*
EOF
	echo "Running \`$*'..."
    echo
	"$@" 2>&1 | tee -a "$tmpfile"
	# Remove color in the log file
	#sed -i -r 's/\x1B\[([0-9]{1,3}(;[0-9]{1,2})?)?[mGK]//g' "$tmpfile"
	echo
	echo "Create a git commit to log the execution..."
	if ! git -C $G5KSPACK_ROOT commit --allow-empty -F "$tmpfile"; then
		echo
		echo "Failed: could not commit to git."
		return 3
	fi
    cat <<EOF

Note: If the command you ran generated some files you wish to associate to the git commit, please do it now with:
  git add <files>
  git commit --amend --no-edit

EOF
}

### g5kspack_install
# Install a package
# Usage: g5kspack_install <package description>
# Options:
# - Set G5KSPACK_NO_SPEC_BEFORE_INSTALL to omit the `spack spec' command
# - Set G5KSPACK_NON_INTERACTIVE to process non interactively
g5kspack_install() {
	if [ -z "$1" ]; then
		echo "No package given."
		echo
		return 1
	fi
	# Prevent installation if spack's git is not clean
	if ! _g5kspack_check_git_clean; then
		return 2
	fi
	# First review the spec of what would be concretized
	if [ -z "$G5KSPACK_NO_SPEC_BEFORE_INSTALL" ]; then
		echo "Running \`spack spec -It $*'"
		spack spec -It "$@"
		if ! _g5kspack_question "Do install? [y/N] " N ${G5KSPACK_NON_INTERACTIVE:+Y}; then
			echo "Aborted!"
			return 3
		fi
	fi
	# Do install and log
	local tmpfile
	tmpfile=$(mktemp /tmp/g5kspack_install.XXXX)
	echo "# g5kspack_install $*" > "$tmpfile"
	echo "$ spack install $*" >> "$tmpfile"
	echo "Running \`spack install $*'"
	if ! SPACK_COLOR=always spack install "$@" | tee -a "$tmpfile"; then
		echo
		echo "Failed."
		return 4
	fi
	# Remove color in the log file
	sed -i -r 's/\x1B\[([0-9]{1,3}(;[0-9]{1,2})?)?[mGK]//g' "$tmpfile"
	# Retrieve the new package name with its hash
	local pkg
	pkg=$(tail -n1 "$tmpfile" | sed -e 's/^\[+\] //')
	# Add the git commit short message to the head of the tmp file
	sed -i "1s/^/g5kspack_install ${pkg##*/}\n\n/" "$tmpfile"
	echo
	# Commit useful installation metadata to git (in case of reinstall from scratch)
	# - spec.json: can be use to reinstall as is with `spack install -f spec.json'
	# - installation_environment.json: gives the system environment used for the install and the spack version which includes the git ref hash
	# Then tag.
	echo "Commit new package installation metadata to git..."
	if ! [ -e "$pkg"/.spack/spec.json ] && ! [ -e "$pkg"/.spack/install_environment.json ]; then
		echo "Failed: metadata not found in $pkg."
		return 5
	fi
	if ! git -C $G5KSPACK_ROOT add -f "$pkg"/.spack/{spec,install_environment}.json ||
		! git -C $G5KSPACK_ROOT commit -F "$tmpfile" ||
		! git -C $G5KSPACK_ROOT tag -f -a -F "$tmpfile" "$G5KSPACK_BRANCH/install_${pkg##*/}"; then
		echo
		echo "Failed: could not commit to git and tag."
		return 6
	fi
	echo
	echo "Installation succeed!"
	echo
	# Refresh modules
	if _g5kspack_question "Refresh modules? [y/N] " N ${G5KSPACK_NON_INTERACTIVE:+N}; then
		g5kspack_refresh_modules
	else
		echo "You may run later \`g5kspack_refresh_modules'."
	fi
}

### g5kspack_uninstall
# Uninstall a package
# Usage: g5kspack_uninstall <package description>
# Set G5KSPACK_NON_INTERACTIVE to process non interactively
g5kspack_uninstall() {
	if [ -z "$1" ]; then
		echo "No package given."
		echo
		return 1
	fi
	if [ -n "$2" ]; then
		echo "More than one argument, but this command only takes one argument:"
		echo "- Only one package can be uninstalled at a time. Uninstall dependent packages beforehand."
		echo "- Provide the package hash if the package name is not sufficient to identify it."
		echo
		return 1
	fi
	# Prevent uninstallation if spack's git is not clean
	if ! _g5kspack_check_git_clean; then
		return 2
	fi
	# Find the exact package to uninstall and fail if many packages match
	local -a pkglookup
	if ! pkglookup=($(spack find --format "{name}-{version}-{hash}" "$1")) || [ -z "${pkglookup[0]}" ] || [ -n "${pkglookup[1]}" ]; then
		echo
		echo "Package lookup failed, try \`spack find -l $1' and make sure to identify a single package."
		return 3
	fi
	local pkg=${pkglookup[0]}
	if ! git ls-files --error-unmatch "$SPACK_ROOT"/opt/spack/*/*/"$pkg"/.spack >& /dev/null; then
		echo "Package $pkg not found it git, it was probably not installed with g5kspack. Abort."
		echo
		return 4
	fi
	# Do uninstall and log
	local tmpfile
	tmpfile=$(mktemp /tmp/g5kspack_uninstall.XXXX)
	echo "# g5kspack_uninstall $1" > "$tmpfile"
	echo "$ spack uninstall $1" >> "$tmpfile"
	echo "Running \`spack uninstall $1'"
	if ! SPACK_COLOR=always spack uninstall "$1" | tee -a "$tmpfile"; then
		echo
		echo "Failed."
		return 5
	fi
	# Remove color in the log file
	sed -i -r 's/\x1B\[([0-9]{1,3}(;[0-9]{1,2})?)?[mGK]//g' "$tmpfile"
	# Add the git commit short message to the head of the tmp file
	sed -i "1s/^/g5kspack_uninstall ${pkg##*/}\n\n/" "$tmpfile"
	echo
	# Commit changes (should only be the removal of the package metadata, and tag.
	if git -C "$G5KSPACK_ROOT" commit -a -F "$tmpfile" &&
		git -C "$G5KSPACK_ROOT" tag -f -a -F "$tmpfile" "$G5KSPACK_BRANCH/uninstall_${pkg##*/}"; then
		echo
		echo "Success!"
	else
		echo
		echo "Failed: could not commit to git and tag."
		return 6
	fi
	echo
	# Refresh modules
	if _g5kspack_question "Refresh modules? [y/N] " N ${G5KSPACK_NON_INTERACTIVE:+N}; then
		g5kspack_refresh_modules
	else
		echo "You may run later \`g5kspack_refresh_modules'."
	fi
}

### g5kspack_setup_other_nodes
# Mount (or umount if 1st argument is "-u") the work copy of the spack hierarchy on some nodes
# Usage: g5kspack_setup_other_nodes [-d] <node1> [<node2> [...]]
g5kspack_setup_other_nodes() {
	local umount
	if [ "$1" == "-u" ]; then
		umount=1
		shift
	fi
	if [ -z "$1" ]; then
		echo "Please provide a node list"
		return 1
	fi
	for n in "$@"; do
		if [ "$n" == "$HOSTNAME" ] || [ "$n" == "${HOSTNAME%%.*}" ]; then
			echo "Do nothing on current node $n!"
			continue
		fi
		echo "* $n:"
		if [ -z "$umount" ]; then
			sudo -u "$ADM_USER" ssh -oStrictHostKeyChecking=no "$n" bash <<EOF
if grep -q "$G5KSPACK_SERVER:$G5KSPACK_NFSEXPORT" /proc/mounts; then
	echo "Already mounted"
else
	echo "Mount $G5KSPACK_SERVER:$G5KSPACK_NFSEXPORT on $G5KSPACK_ROOT"
	sudo-g5k mkdir -p ${G5KSPACK_ROOT}
	sudo-g5k mount -v -t nfs -o ro${G5KSPACK_NFSMOUNT_OPTS:+,$G5KSPACK_NFSMOUNT_OPTS} "$G5KSPACK_SERVER:$G5KSPACK_NFSEXPORT" "$G5KSPACK_ROOT"
	echo Done
fi
EOF
		else
			sudo -u "$ADM_USER" ssh -oStrictHostKeyChecking=no "$n" bash <<EOF
if grep -q "$G5KSPACK_SERVER:$G5KSPACK_NFSEXPORT" /proc/mounts; then
	echo "Umount $G5KSPACK_SERVER:$G5KSPACK_NFSEXPORT"
	sudo-g5k umount -v "$G5KSPACK_SERVER:$G5KSPACK_NFSEXPORT"
	echo Done
else
	echo "Not mounted"
fi
EOF
		fi
	done
}

### g5kspack_new_compiler_target
# Install a new compiler for a target, by first compiling it with Debian's compiler, then recompiling it with itself.
# May be used several time, to add a new compiler
# Usage: g5kspack_new_compiler_targer <compiler> <targer>
g5kspack_new_compiler_target() {
	local spack_compiler=${1:-$G5KSPACK_DEFAULT_COMPILER}
	local spack_target=${2:-$G5KSPACK_DEFAULT_TARGET}
	#apt update && sudo apt install -y nfs-common git-core curl make unzip bzip2 xz-utils libc6-dev binutils patchelf file

	echo "Temporarilly use Debian's gcc."
	echo "Running \`rm -f ~/.spack/compilers.yaml; spack compiler add --scope user /usr/bin'"
	rm -f ~/.spack/compilers.yaml 2> /dev/null
	spack compiler add --scope user /usr/bin || return 1
	local debian_compiler
	debian_compiler=$(grep "spec:" ~/.spack/compilers.yaml | sed -e "s/^ *spec: *\(.*\)/\1/")
	echo
	echo "Install $spack_compiler for $spack_target with Debian's $debian_compiler."
	G5KSPACK_NON_INTERACTIVE=1 G5KSPACK_NO_SPEC_BEFORE_INSTALL=1 g5kspack_install "$spack_compiler%$debian_compiler" target="$spack_target" os="$(spack arch -o)" || return 2
	echo
	echo "Temporarilly set $spack_compiler built with Debian's $debian_compiler as the spack compiler."
	echo "Running \`rm ~/.spack/compilers.yaml && spack compiler add --scope user $(spack location --install-dir "$spack_compiler%$debian_compiler" arch="$spack_target")/bin/'"
	rm ~/.spack/compilers.yaml &&
	spack compiler add --scope user "$(spack location --install-dir "$spack_compiler%$debian_compiler" arch="$spack_target")/bin/" || return 3
	echo
	echo "Rebuild and install $spack_compiler with itself."
	G5KSPACK_NON_INTERACTIVE=1 G5KSPACK_NO_SPEC_BEFORE_INSTALL=1 g5kspack_install "$spack_compiler%$spack_compiler" target="$spack_target" os="$(spack arch -o)" || return 4
	echo
	echo "Mark $spack_compiler compiled with Debian's $debian_compiler as implicite"
	echo "Running \`spack mark -i $spack_compiler %$debian_compiler target=$spack_target os=$(spack arch -o)'"
	spack mark -i "$spack_compiler%$debian_compiler" target="$spack_target" os="$(spack arch -o)" || return 5
	echo
	echo "Set $spack_compiler compiled with itself as the spack compiler"
	echo "Running \`rm ~/.spack/compilers.yaml && spack compiler add --scope site $(spack location --install-dir "$spack_compiler%$spack_compiler" arch="$spack_target")/bin/'"
	rm ~/.spack/compilers.yaml &&
	spack compiler add --scope site "$(spack location --install-dir "$spack_compiler%$spack_compiler" arch="$spack_target")/bin/" || return 6
	echo
	if ! [ -r $G5KSPACK_ROOT/etc/spack/packages.yaml ] || ! grep -q "^\s\+target:.*[[, ]${spack_target}[], ].*$" $G5KSPACK_ROOT/etc/spack/packages.yaml; then
		echo "Mind setting target $spack_target in $G5KSPACK_ROOT/etc/spack/packages.yaml"
		echo
	fi
	echo "Commit to git the new compiler configuration"
	echo "Running \`git -C $G5KSPACK_ROOT add etc/spack && git -C $G5KSPACK_ROOT commit -m \"g5kspack_new_compiler_target $spack_compiler $spack_target\"'"
	git -C $G5KSPACK_ROOT add etc/spack &&
	git -C $G5KSPACK_ROOT commit -m "g5kspack_new_compiler_target $spack_compiler $spack_target" || return 7
	echo
}

###g5kspack_trigger_NFS_servers_sync
# Triggers the deployment of the spack file hierarchy to the NFS servers (multi-site deployment).
g5kspack_trigger_NFS_servers_sync() {
	if [ -n "$G5KSPACK_NFS_SERVERS_SYNC_TRIGGER" ]; then
		$G5KSPACK_SSHCMD "$G5KSPACK_NFS_SERVERS_SYNC_TRIGGER"
	fi
}

### g5kspack_additional_spack_repo
# Add or update a git submodule for an additional spack repository
# Usage: g5kspack_additional_spack_repo <url> [<name>]
#        g5kspack_additional_spack_repo -u <name>
g5kspack_additional_spack_repo() {
	# Prevent run if spack's git is not clean
	if ! _g5kspack_check_git_clean; then
		return 2
	fi
	if [ "$1" == "-u" ]; then
		local name=${2##*/}
		if [ -z "$name" ]; then
			echo "Error: please provide the name of the additional spack repo to update."
			exit 2
		fi
		echo "Fetch submodule updates from the remote repository of $name..."
		git -C $G5KSPACK_ROOT submodule update --remote "var/spack/repos/$name"
		if [ -z "$(git -C $G5KSPACK_ROOT submodule summary "var/spack/repos/$name")" ]; then
			echo "No change found."
		else
			echo "Changes:"
			git -C $G5KSPACK_ROOT submodule summary "var/spack/repos/$name"
			if ! _g5kspack_question "Commit update? [y/N] " N ${G5KSPACK_NON_INTERACTIVE:+Y}; then
				echo "Aborted!"
				return 1
			fi
			git -C $G5KSPACK_ROOT add "var/spack/repos/$name"
			git -C $G5KSPACK_ROOT commit -m "[g5kspack] update the git submodule of the repo $name"
		fi
	else
		local url=$1
		if [ -z "$url" ]; then
			echo "Error: please provide the git clone URL of the additional spack repo to add."
			exit 2
		fi
		local name=${2##*/}
		if [ -z "$name" ]; then
			name=${url##*/}
			name=${name%.git}
		fi
		git -C $G5KSPACK_ROOT submodule add "$url" "var/spack/repos/$name"
		git -C $G5KSPACK_ROOT submodule init "var/spack/repos/$name"
		spack repo add --scope site "$G5KSPACK_ROOT/var/spack/repos/$name"
		git -C $G5KSPACK_ROOT add etc/spack/repos.yaml
		git -C $G5KSPACK_ROOT commit -m "[g5kspack] add spack repo $name as a git submodule"
	fi
}

###g5kspack_init
# Populates the G5KSPACK_ROOT directory with the clone of the spack git repository. Should only be used once.
# The git repository my contains former work (commits) with some configuration files, etc.
# Any commit related to g5kspack_install or g5kspack_uninstall should not be present however.
# Usage: g5kspack_init
g5kspack_init() {
	local existing
	if [ -e $G5KSPACK_ROOT/.git ]; then
		echo "$G5KSPACK_ROOT seems to already be a git repository. Initialization already done? Aborting."
		echo
		return 1
	fi
	if git ls-remote --exit-code -q $G5KSPACK_REMOTE $G5KSPACK_BRANCH > /dev/null; then
		existing=1
		if ! _g5kspack_question "Git branch $G5KSPACK_BRANCH already exists on $G5KSPACK_REMOTE. Use it? [Y/n] " Y ${G5KSPACK_NON_INTERACTIVE:+Y}; then
			echo "Aborted!"
			return 2
		fi
		echo
		git clone -b $G5KSPACK_BRANCH -o origin $G5KSPACK_REMOTE --recursive $G5KSPACK_ROOT
		echo
		echo "Configure spack upstream remote: $G5KSPACK_UPSTREAM_REMOTE"
		git -C $G5KSPACK_ROOT remote add upstream $G5KSPACK_UPSTREAM_REMOTE
		echo
	else
		# if repo exists but branch does not, exit code is 2
		if [ $? != 2 ]; then
			echo
			echo "Error: git repository $G5KSPACK_REMOTE does not exist. Make sure to create it beforehand."
			echo
			return 3
		fi
		# At this point, $G5KSPACK_BRANCH does not exist yet on remote. We create it.
		if ! _g5kspack_question "Git branch $G5KSPACK_BRANCH does not exist on $G5KSPACK_REMOTE. Create it? [Y/n] " Y ${G5KSPACK_NON_INTERACTIVE:+Y}; then
			echo "Aborted!"
			return 2
		fi
		echo
		echo "Forking the $G5KSPACK_UPSTREAM_RELEASE branch from $G5KSPACK_UPSTREAM_REMOTE as the $G5KSPACK_BRANCH branch:"
		git clone -b $G5KSPACK_UPSTREAM_RELEASE -o upstream --single-branch $G5KSPACK_UPSTREAM_REMOTE $G5KSPACK_ROOT
		git -C $G5KSPACK_ROOT branch -m $G5KSPACK_BRANCH
		git -C $G5KSPACK_ROOT remote add origin $G5KSPACK_REMOTE
		echo "Configure the g5kspack tool submodule in $G5KSPACK_ROOT/g5kspack:"
		git -C $G5KSPACK_ROOT submodule add -b $G5KSPACK_TOOL_RELEASE $G5KSPACK_TOOL_REMOTE g5kspack
		git -C $G5KSPACK_ROOT submodule init g5kspack
		git -C $G5KSPACK_ROOT commit -m "[g5kspack] add the g5kspack tool submodule"
		echo
		echo "Finish the initial setup:"
		cat <<EOF >> $G5KSPACK_ROOT/.gitignore

#############################
# g5kspack-specific ignores #
#############################
/bootstrap
!/etc/spack/*

EOF
		git -C $G5KSPACK_ROOT commit -a -m "[g5kspack] add g5kspack-specific ignores to .gitignore"
		mkdir $G5KSPACK_ROOT/bootstrap
		cat <<EOF > $G5KSPACK_ROOT/etc/spack/bootstrap.yaml
bootstrap:
  root: $G5KSPACK_ROOT/bootstrap
EOF
		git -C $G5KSPACK_ROOT add $G5KSPACK_ROOT/etc/spack/bootstrap.yaml
		git -C $G5KSPACK_ROOT commit -m "[g5kspack] add the spack bootstrap configuration"
		cp -v $G5KSPACK_CONFIG $G5KSPACK_ROOT/g5kspack.conf
		git -C $G5KSPACK_ROOT add $G5KSPACK_ROOT/g5kspack.conf
		git -C $G5KSPACK_ROOT commit -m "[g5kspack] add the g5kspack.conf file"
		echo
		if [ "$G5KSPACK_GIT_PUSH" == "yes" ]; then
			echo "Push $G5KSPACK_BRANCH to $G5KSPACK_REMOTE:"
			git -C $G5KSPACK_ROOT push -u origin
			echo
		fi
	fi
	if ! [ -d $G5KSPACK_ROOT/g5kspack ] && ! git -C $G5KSPACK_ROOT submodule status g5kspack >& /dev/null; then
		echo "Error: no git submodule found for the g5kspack tool, please fix."
		echo
		return 3
	fi
	echo "Move out the temporary g5kspack configuration file."
	mv -v $G5KSPACK_CONFIG{,.bak}
	unset G5KSPACK_CONFIG
	echo
	. $G5KSPACK_ROOT/g5kspack/setup.sh
	[ -z "$existing" ] || cat <<'EOF'
!!!WARNING!!!
You cloned an existing spack/g5kspack git repository on an empty ZFS dataset. It can contain initial commits for some configurations already (e.g. files in etc/spack/).
But since this is the initialisation of the ZFS dataset, no commit/tag should refer to any package installation, because nothing is actually installed yet (installed packages actual files are not stored in git but in the ZFS dataset only).

EOF
}

###g5kspack_tool_update
# Update the g5kspack tool submodule
# Usage: g5kspack_tool_update
g5kspack_tool_update() {
	# Prevent run if spack's git is not clean
	if ! _g5kspack_check_git_clean; then
		return 2
	fi
	echo "Fetch submodule updates from the g5kspack tool remote repository..."
	git -C $G5KSPACK_ROOT submodule update --remote g5kspack
	if [ -z "$(git -C $G5KSPACK_ROOT submodule summary g5kspack)" ]; then
		echo "No change found."
	else
		echo "Changes:"
		git -C $G5KSPACK_ROOT submodule summary g5kspack
		if ! _g5kspack_question "Commit update? [y/N] " N ${G5KSPACK_NON_INTERACTIVE:+Y}; then
			echo "Aborted!"
			return 1
		fi
		git -C $G5KSPACK_ROOT add g5kspack
		git -C $G5KSPACK_ROOT commit -m "[g5kspack] update the submodule of the g5kspack tool"
		echo "Update functions:"
		echo
		. $G5KSPACK_ROOT/g5kspack/setup.sh
	fi
}

cat <<'EOF'
Environment is ready with the following helper functions defined:

Work environement functions
  g5kspack_begin_work                                  : create and mount a work copy of the spack environement
  g5kspack_end_work                                    : unmount and destroy the work copy
  g5kspack_take_over_work                              : take over a previous work
  g5kspack_setup_other_nodes [-u] <node> [<node>...]   : configure some other nodes to use the work copy (-u to unconfigure)
  g5kspack_commit_work                                 : commit changes made in the work copy

Enhanced Spack functions
  g5kspack_install <package>                           : install a spack package in the work copy
  g5kspack_uninstall <package>                         : uninstall a spack package in the work copy
  g5kspack_new_compiler_arch <compiler> <arch>         : install a new compiler/architecture target in the work copy
  g5kspack_additional_spack_repo <url> [<name>]        : add an additional spack repo
  g5kspack_additional_spack_repo -u <name>             : update an additional spack repo
  g5kspack_refresh_modules                             : refresh the spack managed environment modules
  g5kspack_run <command>                               : run an arbitrary command and log its stdout and stder in a git commit

Other functions
  g5kspack_init                                        : initialize the work copy (use if reinstalling everything from scratch)
  g5kspack_tool_update                                 : update the g5kspack tool
  g5kspack_trigger_NFS_servers_sync                    : triggers the deployment of the spack file hierarchy to the NFS servers (multi-site deployment)
  g5kspack_rollback_git_origin                         : rollback the git remote to the current local state on the ZFS snapshot (possibly after a ZFS rollback)

EOF
if [ -n "$_WARNING" ]; then
	echo "WARNING: $_WARNING"
	echo
	unset _WARNING
fi
