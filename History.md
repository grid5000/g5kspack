# Identifying g5k-specific changes in spack

Our fork of the spack repository is here: https://gitlab.inria.fr/grid5000/spack, and the current default branch is `g5k/v1`.
In addition to various patches to spack's configuration, we also log all our package installation and removal.
Periodically, we would update our default branch to merge the upstream's `develop` branch, which corresponds to the latest version of spack.

It may be hard to see right away all our changes with regards to upstream spack, and the following two sections aim at easily identifying the changes we've made, to the source code of spack, and to the installed packages themselves.

## Spack patches and configuration changes

The easiest way to have an overall view on all the changes compared to upstream is basically to make the "correct" `git diff` call.
First we need to identify the last time `upstream/develop` was merged into our `g5k/v1`.
At the time of writing this documentation, the latest commit from `upstream/develop` merged into `g5k/v1` is `7440bb4c36fb2482b86abdf3bf90db6123cf9269`.

The repository include some json with the specs of all the packages we've installed; those can be safely ignored for this section, and the appropriate git command should be:

```bash
git diff 7440bb4c36fb2482b86abdf3bf90db6123cf9269 :^opt/spack
```

It will output a diff; the smaller it is, the happier we are, but there will always inevitably be some configuration changes.

I (Philippe) have exported the diff at the time of writing that documentation and attached it to [the original ticket](https://intranet.grid5000.fr/bugzilla/show_bug.cgi?id=15401).


## History of packages installation/removal

When using g5kspack helpers, all the actions are logged in commits.
The easiest way to get all commands ran by g5k staff is to filter the git log and grep for `g5kspack_*` usage; and this should give what we're looking for:

```bash
git log | grep '# g5k' | sed 's/    # //'
```

I have attached all actions performed by g5k staff at the time of writing that documentation in [the original ticket](https://intranet.grid5000.fr/bugzilla/show_bug.cgi?id=15401).
